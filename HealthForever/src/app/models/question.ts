import { Option } from './option';

export class Question {
    id: number;
    name: string;
    answertype: string;
    options: Option[];
    condition: string;//condition
    answered: boolean;
    photoSource: string;
    userAns: string;
    score: number;
    optionId: number;

    question1:string;
    answerType:string;
    possibleValues:string;
    conditionId:number;

    attachedDocuments:File[]=new Array();

    label;
    inputType;
    isRequired;
    placeholder;
    pattern;
    defaultValue;


    constructor(data: any) {
        data = data || {};
        this.id = data.questionId;
        this.name = data.question;
        this.answertype = data.answerType;
        this.options = [];
        this.condition = data.condition;
        this.photoSource = data.photoSource;
        this.userAns = data.defaultValue;
        this.optionId= data.optionId;

        this.question1=data.question1;
        this.answerType=data.answerType;
        this.possibleValues=data.possibleValues;
        this.conditionId=data.conditionId;

        this.label=data.label;
        this.inputType=data.inputType;
        this.isRequired=data.isRequired;
        this.placeholder=data.placeholder;
        this.pattern=data.pattern;
        this.defaultValue=data.defaultValue;
       
         data.options.forEach(o => {  
                
             this.options.push(new Option(o));
         });
    }
}
