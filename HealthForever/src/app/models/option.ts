export class Option {
    id: number;
    questionId: number;
    name: string;
    isAnswer: boolean;
    selected: boolean;
    score: number;
    answeroption: string;
    questionOptionId:number;

    constructor(data: any) {
        data = data || {};
        this.id = data.optionId;
        this.name = data.option;
        this.questionId = data.questionId;
        this.score = data.score;
        this.answeroption=data.answeroption;
        this.questionOptionId=data.questionOptionId;
    }
}
