export class User{
    userId:string;
    age:number;
    email:string;
    mobile:string;
    name:string;
    password:string;
    userName:string;
    userType:string;
}