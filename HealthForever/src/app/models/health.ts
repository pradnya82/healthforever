import { healthConfig } from './health-config';
import { Question } from './question';
import { person } from './person';

export class health {
    id: number;
    name: string;
    description: string;
    config: healthConfig;
    questions: any[];
    answered: any[];
    person: person[];
    userName: string;
    mobileNo: Number;

    constructor(data: any) {
        if (data) {    
            this.userName= "";
            this.mobileNo= 0;
            this.id = data.questionId;           
            this.name = data.responseMessage;
            this.config = new healthConfig(data.config);
            this.questions = [];
            this.answered = [];
            this.person = [];
            

            data.response.forEach(q => {
                this.questions.push(new Question(q));
            });
        }
    }
}
