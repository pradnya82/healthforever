export class Condition{
    conditionId:number;
    condition1:string;
    applicableGender:string;
    applicableMinAge:number;
    applicableMaxAge:number;
}