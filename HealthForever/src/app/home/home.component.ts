import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ConditionService } from '../services/condition.service';
import { Subscription } from 'rxjs';
import { Condition } from '../models/Condition';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,OnDestroy {
  condition=1;
  message  = "Stay Safe";
  
  constructor(private router: Router,private route: ActivatedRoute,
             private http: HttpClient,
             private conditionService:ConditionService) { }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  subscription:Subscription=new Subscription();
  conditions:Condition[]=new Array();

  ngOnInit() {
    
    this.subscription.add(
      this.conditionService.getAll().subscribe(
        res=>{
          this.conditions=res;
        }
      )
    )

    console.log("this home page is loaded using snapshot for " + this.condition)
  }

  onClick(){
    this.router.navigate(['/health/'+this.condition]);
  }

}
