import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';

@Injectable()
export class GlobalConstants {

    public apiURL: string = "https://itsolutionstuff.com/";
    public siteTitle: string = "This is example of ItSolutionStuff.com";
    message: String ="Stay Home, Stay Healthy"
  static message: any;

}