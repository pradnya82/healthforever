import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { FormGroup, FormControl, Validators} from '@angular/forms';
@Component({
  selector: 'app-uploadfile',
  templateUrl: './uploadfile.component.html',
  styleUrls: ['./uploadfile.component.css']
})
export class UploadfileComponent implements OnInit {

  myForm = new FormGroup({

    name: new FormControl('', [Validators.required, Validators.minLength(3)]),

    file: new FormControl('', [Validators.required]),

    fileSource: new FormControl('', [Validators.required])

  });

    
 ngOnInit() {
 }
  constructor(private http: HttpClient) { }

      

  get f(){

    return this.myForm.controls;

  }

     

  onFileChange(event) {

  

    if (event.target.files.length > 0) {

      const file = event.target.files[0];

      this.myForm.patchValue({

        fileSource: file

      });

    }

  }

     

  submit(){

    const formData = new FormData();

    formData.append('file', this.myForm.get('fileSource').value);

   

    this.http.post('http://localhost:8080/quiz/rest/file/uploadFile', formData)

      .subscribe(res => {

        console.log(res);
        alert(res['fileDownloadUri']);
        alert('Uploaded Successfully.');

      })

  }
}
