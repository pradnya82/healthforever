import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadmultifilesComponent } from './uploadmultifiles.component';

describe('UploadmultifilesComponent', () => {
  let component: UploadmultifilesComponent;
  let fixture: ComponentFixture<UploadmultifilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadmultifilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadmultifilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
