import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';


@Injectable({
  providedIn: 'root'
})
export class ConditionService {

  
  constructor(
    private httpService:HttpClient,
    private sharedService:SharedService) { }
    model = "conditions/"
    urlName = "healthcareUrl";
    

  getAll():Observable<any>{
    var url = this.sharedService.config.getConfig("beURL") + this.model + "allValid/client";
    return this.httpService.get(url);
  }

}
