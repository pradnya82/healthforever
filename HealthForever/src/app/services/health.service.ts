import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';

@Injectable()
export class healthService {

  constructor(private http: HttpClient,
    private sharedService:SharedService) { }

    component = "questions/";

  getQuestions(condition:string):Observable<any>{
    
    let Url = this.sharedService.config.getConfig('beURL') +this.component+'QuestionsByCondition/' + condition;
              
    return this.http.get(Url);
  }

  getAnswered(data:any):Observable<any>{
    let Url = this.sharedService.config.getConfig('beURL')+this.component+'saveAnswers';
    return this.http.post(Url,data);
  }

  uploadDocument(file:File){
    var formData:FormData=new FormData();
    formData.append("file",file);

    let Url = this.sharedService.config.getConfig('beURL')+this.component+'uploadDocument';
    return this.http.post(Url,formData);
  }
  
  get(url: string) {
    return this.http.get(url);
  }

  getNextAct(nextMove:string){
    let Url = this.sharedService.config.getConfig('beURL')+nextMove;
              
    return this.http.get(Url);
  }

  getAll() {
    return [
      { id: 'data/javascript.json', name: 'JavaScript' },//http://13.232.40.254:8080/quiz/rest/question/corona
      { id: 'data/aspnet.json', name: 'Asp.Net' },
      { id: 'data/csharp.json', name: 'C Sharp' },
      { id: 'data/designPatterns.json', name: 'Design Patterns' }
    ];
  }

  public uploadImages(formData) {
    let uploadURL=this.sharedService.config.getConfig('beURL') + "file/uploadMultipleFiles"    
    return this.http.post<any>(uploadURL, formData, {  
        reportProgress: true,  
        observe: 'events'  
      });  
  }
  
}
