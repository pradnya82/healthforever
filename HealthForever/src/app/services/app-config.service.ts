import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AppConfigService {

  private config: Object = null;

  constructor(private httpService: HttpClient) { }
  /**
   * Use to get the data found in the second file (config file)
   */
  public getConfig(key: any) {
    return this.config[key];
  }

 /**
   * This method:
   *   b) Loads "config.json"
   */
  public load() {
    return new Promise((resolve, reject) => {
        this.httpService.get("./assets/config/config.json").subscribe(
          res=>{
            console.log(res);
            this.config = res;
            resolve(true);
          },
          err=>{
            console.error('Config file is not valid');
            resolve(true);
            console.log(err);
            // this.errorHandler.showErrorMessage("Enrollment Failed");
          }

      );
    });
  }
}
