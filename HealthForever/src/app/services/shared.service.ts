import { Injectable } from '@angular/core';
import { AppConfigService } from './app-config.service';
import {Title} from "@angular/platform-browser";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(public config: AppConfigService,
    private titleService:Title
    ) { 
      this.titleService.setTitle(this.config.getConfig("applicationTitle"));
    }
}
