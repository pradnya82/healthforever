// Provides snackBar configuration globally

import { MatSnackBar } from '@angular/material';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class snackbarExtended{
    constructor( private snackBar : MatSnackBar) {}
    
// this function will open up snackbar on top position with custom background color (defined in css)
openSnackBar(message: string) {

    this.snackBar.open(message, undefined, {
     duration: 3000,
     verticalPosition: 'top',
     panelClass: ['white-snackbar'],
   });
}
}