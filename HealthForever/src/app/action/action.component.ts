import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent implements OnInit {
  condition ='';
  actionMessage  = "Stay Safe";
  
  constructor(private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
    this.actionMessage= this.route.snapshot.params.message;
    console.log("message from previous page " + this.actionMessage);
    this.condition = this.route.snapshot.params.condition;
    console.log("this home page is loaded using snapshot for " + this.condition);
  }

  onClick(){
    this.router.navigate(['/health', this.condition]);

  }
}
