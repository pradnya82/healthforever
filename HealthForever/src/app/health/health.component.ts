import { Component, OnInit } from '@angular/core';

import { healthService } from '../services/health.service';
import { HelperService } from '../services/helper.service';
import { Option, Question, health, healthConfig } from '../models/index';
import { HttpClient, HttpEventType, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { MatRadioChange, MatDialog } from '@angular/material';
import { isGeneratedFile } from '@angular/compiler/src/aot/util';
import { assertNotNull, nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';
import { UserComponent } from '../user/user.component';
import { element } from 'protractor';
import { Router } from '@angular/router';
import { snackbarExtended } from '../services/SnackbarExtended';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../models/User';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { SharedService } from '../services/shared.service';
//import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/observable/combineLatest';

//import { UserComponent } from '../user/user.component';

//import { MatRadioButton, MatRadioGroup } from '@angular/material/radio';


@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.css'],
  providers: [healthService]
})
export class healthComponent implements OnInit {

  user:User=new User();

  nextAct: boolean = false;
  nextMove: string;
  nextMessage: string;
  healthes: any[];
  health: health = new health(null);
  mode = 'health';
  healthName: string;
  answertype: string;
  config: healthConfig = {
    'allowBack': true,
    'allowReview': true,
    'autoMove': false,  // if true, it will move to next question automatically when answered.
    'duration': 300,  // indicates the time (in secs) in which health needs to be completed. 0 means unlimited.
    'pageSize': 1,
    'requiredAll': false,  // indicates if you must answer all the questions before submitting.
    'richText': false,
    'shuffleQuestions': false,
    'shuffleOptions': false,
    'showClock': false,
    'showPager': true,
    'theme': 'none'
  };
  //params: Params;
  message: string = "Stay at home, Stay safe"
  condition: string = 'general';
  numOfQues: number;
  pager = {
    index: 0,
    size: 1,
    count: 1
  };

  filesToSendBE: File[] = new Array();

  sMsg: string = '';

  fileData: File = null;
  previewUrl: any = null;
  fileUploadProgress: string = null;
  uploadedFilePath: string = null;
 
  currentURL: string;
  userId: number = Number(this.cookieService.get("userId"));

  constructor(private healthService: healthService,
    private router: Router,
    private http: HttpClient,
    private snackBar: snackbarExtended,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private cookieService: CookieService,
    private localStorage:LocalStorage,
    public sharedService:SharedService
  ) { }


  ngOnInit() {

    this.route.paramMap.subscribe(params => {
      this.condition = params.get("condition") // Subscription param
      console.log("this page is loaded using subscribe for " + this.condition)
      this.localStorage.getItem("user").subscribe(
        res=>{
          this.user=res;
        }
      )
     

    })
    //this.condition = this.route.snapshot.params.condition;
    this.healthes = this.healthService.getAll();
    this.healthName = this.healthes[0].id;
    this.loadhealth(this.healthName);
  }

  loadhealth(healthName: string) {
    this.pager.index = 0;
    this.healthService.getQuestions(this.condition).subscribe(res => {
      this.health = new health(res);
      console.log(res);
      this.pager.count = this.health.questions.length;
      this.numOfQues = this.health.questions.length

    }, error => {
      console.log(error);
    });
    // this.healthService.get(this.healthName).subscribe(res => {
    //   this.health = new health(res);

    //   this.pager.count = this.health.questions.length;
    // });
    this.mode = 'health';
  }

  get filteredQuestions() {
    return (this.health.questions) ?
      this.health.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
  }

  onSelect(question: Question, option: Option) {
    this.answertype = question.answertype;

    let optionName: string = option.answeroption + ' $';
    question.userAns = question.userAns ? question.userAns : '';

    if (option.selected == true) {
      console.log(option);

      if (question.userAns.search(optionName) === -1) {

        question.userAns += optionName;
      }
    } else {
      question.userAns = question.userAns.replace(optionName, "")
    }

    // if (this.config.autoMove) {
    //   this.goTo(this.pager.index + 1);
    // }
  }
  goTo(index: number) {
    if (index >= 0 && index < this.pager.count) {
      this.pager.index = index;
      this.mode = 'health';
    }
  }

  onSubmit() {

    var promise: Promise<any>[] = new Array();
    var questionDocumentRelation:any[]=new Array();
    var docId=0;
    this.health.questions.forEach(q => {
      var question: Question = q;
      // alert("Question : "+question.id)
      // alert("Question documents : "+question.attachedDocuments.length)
      if (question.attachedDocuments && question.attachedDocuments.length>0) {
        question.attachedDocuments.forEach(doc => {
          promise.push(this.healthService.uploadDocument(doc).toPromise());
          questionDocumentRelation[docId++]=question.id;
        });
      }
    });

    Promise.all(promise).then(values => {
      var i = 0;
      values.forEach(sucDoc => {
        var questionForSucDoc = questionDocumentRelation[i++];
         this.health.questions.forEach(q => {

            var question: Question = q;
            
            if(!question.userAns)
            {
              question.userAns="";
            }
            
            if(question.id==questionForSucDoc)
            {
              if (question.attachedDocuments && question.attachedDocuments.length>0) {
                question.userAns += sucDoc.response.DocumentName + ";"
                }
            }
        });
      });


      console.log("will submimt data now");
      let data = { condition: +this.condition, user: User, answers: null }

      if(!this.user){
        this.user=new User();
      }
      

      if (this.cookieService.get("userId") == null) { }
      const dialogRef = this.dialog.open(UserComponent, {
        width: '400px',
        data: { user: this.user }, 
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log(result);
        if(result)
        {
        
        this.nextAct = true;
        
        data.user = result;
        this.user = result;

        this.localStorage.setItem("user",this.user).subscribe(
          res=>{

          }
        )
        
        data.answers = this.health.questions.map(element => {
          return {
            questionId: +element.id,
            optionId: +element.optionId,
            answer: element.userAns,
            score: +element.score
          }
        })

        console.log("Data : ");
        
        console.log(data);

        console.log("ansers api called");
        this.healthService.getAnswered(data).subscribe(response => {

          console.log(response);
          this.snackBar.openSnackBar(response['responseMessage']);
          this.nextMessage = response['responseMessage'];
          this.nextMove = response['nextAction'];
          this.userId = response['userId'];
          this.cookieService.set("userId", String(this.userId));
          console.log("from response:" + this.nextMove + "," + this.nextMessage);
          if (this.nextMove == "undefined" || this.nextMove == "none") {
            console.log("next action not available");
            this.router.navigate(['/noaction', this.message]);
            //
          } else {
            console.log("next action available");
            this.router.navigate(['/action', this.nextMove, this.nextMessage]);

          }
        }, error => {
          console.log(error);
        });
      }
    });
      
      console.log("only files api will be called");
      /*     this.http.post('http://localhost:8080/quiz/rest/file/uploadMultipleFiles', this.frmData).subscribe(
            data => {
              // SHOW A MESSAGE RECEIVED FROM THE WEB API.
              this.sMsg = data as string;
              console.log ("only files uploaded successfully" + this.sMsg);
              
            },
            (err: HttpErrorResponse) => {
              console.log (err.message);    // Show error, if any.
            } 
          ); */


    });
    return;
  }

  backToHome() {
    return this.router.navigateByUrl("home");
  }


  nextAction() {
    if (this.nextMove != "") {
      this.healthService.getNextAct(this.nextMove).subscribe(res => {
        this.nextAct = false;
        console.log(res);
        this.condition = this.nextMove;
        console.log("new condition:" + this.condition);
        this.router.navigate(['/action', this.condition, this.message]);
        //this.health = new health(res);
        // this.pager.count = this.health.questions.length;
      });
    }
  }




  getFileDetails(e, question: Question) {
    for (var i = 0; i < e.target.files.length; i++) {
      question.attachedDocuments.push(e.target.files[i]);
    }
  }

}
