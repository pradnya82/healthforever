/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { healthComponent } from './health.component';

healthComponent

describe('healthComponent', () => {
  let component: healthComponent;
  let fixture: ComponentFixture<healthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ healthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(healthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
