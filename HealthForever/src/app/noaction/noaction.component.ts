import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-noaction',
  templateUrl: './noaction.component.html',
  styleUrls: ['./noaction.component.css']
})
export class NoactionComponent implements OnInit {
  condition ='';
  actionMessage  = "Stay Safe";
  
  constructor(private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
    this.actionMessage= this.route.snapshot.params.message;
    console.log("message from previous page " + this.actionMessage);
   }

  
}
