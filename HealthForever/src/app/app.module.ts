import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { healthComponent } from './health/health.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatProgressBarModule } from '@angular/material';

import { MatInputModule, MatRadioModule,MatCardModule, MatButtonModule, MatSelectModule, MatIconModule, MatTooltipModule } from '@angular/material';
import { MatFormFieldModule, MatSnackBarModule, MatDialogModule } from '@angular/material';

import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';

import { ActionComponent } from './action/action.component';
import { NoactionComponent } from './noaction/noaction.component';
import { CookieService } from 'ngx-cookie-service';
import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { UploadmultifilesComponent } from './uploadmultifiles/uploadmultifiles.component';
import { AppConfigService } from './services/app-config.service';

export function initConfig(config: AppConfigService) {
  return () => config.load();
}


@NgModule({
  declarations: [
    AppComponent,
    healthComponent,
    UserComponent,
    HomeComponent,
    ActionComponent,
    NoactionComponent,
    UploadfileComponent,
    UploadmultifilesComponent
  ],
  entryComponents: [UserComponent],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatInputModule,
    MatFormFieldModule,
    MatDialogModule,
    BrowserModule,  
    AppRoutingModule,  
    HttpClientModule,  
    BrowserAnimationsModule,  
    MatToolbarModule,  
    MatIconModule,  
    MatButtonModule,  
    MatCardModule,  
    MatProgressBarModule,
    MatToolbarModule,
    MatSelectModule 
    ],
  providers: [CookieService,
    AppConfigService,
    { 
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [AppConfigService],
      multi: true 
    },],
  bootstrap: [AppComponent]
})
export class AppModule { }
