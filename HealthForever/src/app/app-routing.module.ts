import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { healthComponent } from './health/health.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { ActionComponent } from './action/action.component';
import { NoactionComponent } from './noaction/noaction.component';
import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { UploadmultifilesComponent } from './uploadmultifiles/uploadmultifiles.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'home/:condition', component: HomeComponent },

  { path: 'action/:condition/:message', component: ActionComponent },
  { path: 'noaction/:message', component: NoactionComponent },
  { path: 'health', component: healthComponent },
  { path: 'health/:condition', component: healthComponent },
  { path: 'user', component: UserComponent },
  { path: 'uploadfile', component: UploadfileComponent },
  { path: 'uploadmultifiles', component: UploadmultifilesComponent }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  // imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
