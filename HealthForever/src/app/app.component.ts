import { Component, OnInit } from '@angular/core';

import { healthComponent } from './health/health.component';
import { Router } from '@angular/router';
import { SharedService } from './services/shared.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private router: Router,
    public sharedService:SharedService){}

  ngOnInit() {}
}
