"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var forms_1 = require("@angular/forms");
var UploadfileComponent = /** @class */ (function () {
    function UploadfileComponent(http) {
        this.http = http;
        this.myForm = new forms_1.FormGroup({
            name: new forms_1.FormControl('', [forms_1.Validators.required, forms_1.Validators.minLength(3)]),
            file: new forms_1.FormControl('', [forms_1.Validators.required]),
            fileSource: new forms_1.FormControl('', [forms_1.Validators.required])
        });
    }
    UploadfileComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(UploadfileComponent.prototype, "f", {
        get: function () {
            return this.myForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    UploadfileComponent.prototype.onFileChange = function (event) {
        if (event.target.files.length > 0) {
            var file = event.target.files[0];
            this.myForm.patchValue({
                fileSource: file
            });
        }
    };
    UploadfileComponent.prototype.submit = function () {
        var formData = new FormData();
        formData.append('file', this.myForm.get('fileSource').value);
        this.http.post('http://localhost:8080/quiz/rest/file/uploadFile', formData)
            .subscribe(function (res) {
            console.log(res);
            alert(res['fileDownloadUri']);
            alert('Uploaded Successfully.');
        });
    };
    UploadfileComponent = __decorate([
        core_1.Component({
            selector: 'app-uploadfile',
            templateUrl: './uploadfile.component.html',
            styleUrls: ['./uploadfile.component.css']
        }),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], UploadfileComponent);
    return UploadfileComponent;
}());
exports.UploadfileComponent = UploadfileComponent;
//# sourceMappingURL=uploadfile.component.js.map