"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var health_service_1 = require("../services/health.service");
var index_1 = require("../models/index");
var http_1 = require("@angular/common/http");
var material_1 = require("@angular/material");
var user_component_1 = require("../user/user.component");
var router_1 = require("@angular/router");
var SnackbarExtended_1 = require("../services/SnackbarExtended");
var router_2 = require("@angular/router");
var ngx_cookie_service_1 = require("ngx-cookie-service");
var healthComponent = /** @class */ (function () {
    function healthComponent(healthService, router, http, snackBar, dialog, route, cookieService) {
        this.healthService = healthService;
        this.router = router;
        this.http = http;
        this.snackBar = snackBar;
        this.dialog = dialog;
        this.route = route;
        this.cookieService = cookieService;
        this.frmData = new FormData();
        this.nextAct = false;
        this.health = new index_1.health(null);
        this.mode = 'health';
        this.config = {
            'allowBack': true,
            'allowReview': true,
            'autoMove': false,
            'duration': 300,
            'pageSize': 1,
            'requiredAll': false,
            'richText': false,
            'shuffleQuestions': false,
            'shuffleOptions': false,
            'showClock': false,
            'showPager': true,
            'theme': 'none'
        };
        //params: Params;
        this.message = "Stay at home, Stay safe";
        this.condition = 'general';
        this.pager = {
            index: 0,
            size: 1,
            count: 1
        };
        this.myFiles = [];
        this.sMsg = '';
        this.fileData = null;
        this.previewUrl = null;
        this.fileUploadProgress = null;
        this.uploadedFilePath = null;
        this.username = this.cookieService.get("userName");
        this.mobileNo = this.cookieService.get("mobileNo");
        this.userId = Number(this.cookieService.get("userId"));
    }
    healthComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.condition = params.get("condition"); // Subscription param
            console.log("this page is loaded using subscribe for " + _this.condition);
            console.log("user id from cookie " + _this.cookieService.get("userId"));
            console.log("user name from cookie " + _this.cookieService.get("userName"));
        });
        //this.condition = this.route.snapshot.params.condition;
        this.healthes = this.healthService.getAll();
        this.healthName = this.healthes[0].id;
        this.loadhealth(this.healthName);
    };
    healthComponent.prototype.loadhealth = function (healthName) {
        var _this = this;
        this.pager.index = 0;
        this.healthService.getQuestions(this.condition).subscribe(function (res) {
            _this.health = new index_1.health(res);
            console.log(res);
            _this.pager.count = _this.health.questions.length;
            _this.numOfQues = _this.health.questions.length;
        }, function (error) {
            console.log(error);
        });
        // this.healthService.get(this.healthName).subscribe(res => {
        //   this.health = new health(res);
        //   this.pager.count = this.health.questions.length;
        // });
        this.mode = 'health';
    };
    Object.defineProperty(healthComponent.prototype, "filteredQuestions", {
        get: function () {
            return (this.health.questions) ?
                this.health.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
        },
        enumerable: true,
        configurable: true
    });
    healthComponent.prototype.onSelect = function (question, option) {
        this.answertype = question.answertype;
        var optionName = option.name + ' $';
        question.userAns = question.userAns ? question.userAns : '';
        if (option.selected == true) {
            console.log(option);
            if (question.userAns.search(optionName) === -1) {
                question.userAns += optionName;
            }
        }
        else {
            question.userAns = question.userAns.replace(optionName, "");
        }
        // if (this.config.autoMove) {
        //   this.goTo(this.pager.index + 1);
        // }
    };
    healthComponent.prototype.goTo = function (index) {
        if (index >= 0 && index < this.pager.count) {
            this.pager.index = index;
            this.mode = 'health';
        }
    };
    healthComponent.prototype.onSubmit = function () {
        var _this = this;
        var data = { condition: this.condition, user: { name: null, mobile: null, userId: null }, answers: null };
        if (this.cookieService.get("userId") == null) { }
        var dialogRef = this.dialog.open(user_component_1.UserComponent, {
            width: '400px',
            data: { username: this.username, mobileNo: this.mobileNo }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.nextAct = true;
            var item = result.split(',');
            data.user.name = item[0];
            data.user.mobile = item[1];
            //  data.user.userId=this.cookieService.get("userId");
            _this.cookieService.set("userName", data.user.name);
            _this.cookieService.set("mobileNo", data.user.mobile);
            data.answers = _this.health.questions.map(function (element) {
                return {
                    questionId: element.id,
                    optionId: element.optionId,
                    answer: element.userAns,
                    score: element.score
                };
            });
            console.log(data);
            _this.frmData.append("userAnswers", JSON.stringify(data));
            _this.healthService.getAnswered(_this.frmData).subscribe(function (response) {
                console.log(response);
                _this.snackBar.openSnackBar(response['responseMessage']);
                _this.nextMessage = response['responseMessage'];
                _this.nextMove = response['nextAction'];
                _this.userId = response['userId'];
                _this.cookieService.set("userId", String(_this.userId));
                console.log("from response:" + _this.nextMove + "," + _this.nextMessage);
                if (_this.nextMove == "undefined" || _this.nextMove == "none") {
                    console.log("next action not available");
                    _this.router.navigate(['/noaction', _this.message]);
                }
                else {
                    console.log("next action available");
                    _this.router.navigate(['/action', _this.nextMove, _this.nextMessage]);
                }
            }, function (error) {
                console.log(error);
            });
        });
        this.http.post('http://localhost:8080/quiz/rest/file/uploadMultipleFiles', this.frmData).subscribe(function (data) {
            // SHOW A MESSAGE RECEIVED FROM THE WEB API.
            _this.sMsg = data;
            console.log("files uploaded successfully" + _this.sMsg);
        }, function (err) {
            console.log(err.message); // Show error, if any.
        });
        return;
    };
    healthComponent.prototype.backToHome = function () {
        return this.router.navigateByUrl("home");
    };
    healthComponent.prototype.nextAction = function () {
        var _this = this;
        if (this.nextMove != "") {
            this.healthService.getNextAct(this.nextMove).subscribe(function (res) {
                _this.nextAct = false;
                console.log(res);
                _this.condition = _this.nextMove;
                console.log("new condition:" + _this.condition);
                _this.router.navigate(['/action', _this.condition, _this.message]);
                //this.health = new health(res);
                // this.pager.count = this.health.questions.length;
            });
        }
    };
    healthComponent.prototype.getFileDetails = function (e) {
        //console.log (e.target.files);
        for (var i = 0; i < e.target.files.length; i++) {
            this.myFiles.push(e.target.files[i]);
        }
    };
    healthComponent.prototype.uploadFiles = function () {
        // const frmData = new FormData();
        for (var i = 0; i < this.myFiles.length; i++) {
            this.frmData.append("files", this.myFiles[i]);
        }
        console.log("files attached" + this.frmData);
    };
    healthComponent = __decorate([
        core_1.Component({
            selector: 'app-health',
            templateUrl: './health.component.html',
            styleUrls: ['./health.component.css'],
            providers: [health_service_1.healthService]
        }),
        __metadata("design:paramtypes", [health_service_1.healthService,
            router_1.Router,
            http_1.HttpClient,
            SnackbarExtended_1.snackbarExtended,
            material_1.MatDialog,
            router_2.ActivatedRoute,
            ngx_cookie_service_1.CookieService])
    ], healthComponent);
    return healthComponent;
}());
exports.healthComponent = healthComponent;
//# sourceMappingURL=health.component.js.map