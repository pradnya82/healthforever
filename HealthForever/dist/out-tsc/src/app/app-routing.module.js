"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var health_component_1 = require("./health/health.component");
var user_component_1 = require("./user/user.component");
var home_component_1 = require("./home/home.component");
var action_component_1 = require("./action/action.component");
var noaction_component_1 = require("./noaction/noaction.component");
var uploadfile_component_1 = require("./uploadfile/uploadfile.component");
var uploadmultifiles_component_1 = require("./uploadmultifiles/uploadmultifiles.component");
var routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: home_component_1.HomeComponent },
    { path: 'home/:condition', component: home_component_1.HomeComponent },
    { path: 'action/:condition/:message', component: action_component_1.ActionComponent },
    { path: 'noaction/:message', component: noaction_component_1.NoactionComponent },
    { path: 'health', component: health_component_1.healthComponent },
    { path: 'health/:condition', component: health_component_1.healthComponent },
    { path: 'user', component: user_component_1.UserComponent },
    { path: 'uploadfile', component: uploadfile_component_1.UploadfileComponent },
    { path: 'uploadmultifiles', component: uploadmultifiles_component_1.UploadmultifilesComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(routes)],
            // imports: [RouterModule.forRoot(routes, { useHash: true })],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app-routing.module.js.map