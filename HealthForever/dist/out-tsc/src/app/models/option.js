"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Option = /** @class */ (function () {
    function Option(data) {
        data = data || {};
        this.id = data.optionId;
        this.name = data.option;
        this.questionId = data.questionId;
        this.score = data.score;
    }
    return Option;
}());
exports.Option = Option;
//# sourceMappingURL=option.js.map