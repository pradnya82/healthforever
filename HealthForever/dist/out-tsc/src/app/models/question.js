"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var option_1 = require("./option");
var Question = /** @class */ (function () {
    function Question(data) {
        var _this = this;
        data = data || {};
        this.id = data.questionId;
        this.name = data.question;
        this.answertype = data.answerType;
        this.options = [];
        this.condition = data.condition;
        this.photoSource = data.photoSource;
        this.userAns = data.userAns;
        this.optionId = data.optionId;
        data.options.forEach(function (o) {
            _this.options.push(new option_1.Option(o));
        });
    }
    return Question;
}());
exports.Question = Question;
//# sourceMappingURL=question.js.map