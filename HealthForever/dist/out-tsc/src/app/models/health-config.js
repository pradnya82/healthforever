"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var healthConfig = /** @class */ (function () {
    function healthConfig(data) {
        data = data || {};
        this.allowBack = data.allowBack;
        this.allowReview = data.allowReview;
        this.autoMove = data.autoMove;
        this.duration = data.duration;
        this.pageSize = data.pageSize;
        this.requiredAll = data.requiredAll;
        this.richText = data.richText;
        this.shuffleQuestions = data.shuffleQuestions;
        this.shuffleOptions = data.shuffleOptions;
        this.showClock = data.showClock;
        this.showPager = data.showPager;
    }
    return healthConfig;
}());
exports.healthConfig = healthConfig;
//# sourceMappingURL=health-config.js.map