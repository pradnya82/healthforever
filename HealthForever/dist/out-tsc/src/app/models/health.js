"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var health_config_1 = require("./health-config");
var question_1 = require("./question");
var health = /** @class */ (function () {
    function health(data) {
        var _this = this;
        if (data) {
            this.userName = "";
            this.mobileNo = 0;
            this.id = data.questionId;
            this.name = data.responseMessage;
            this.config = new health_config_1.healthConfig(data.config);
            this.questions = [];
            this.answered = [];
            this.person = [];
            data.response.forEach(function (q) {
                _this.questions.push(new question_1.Question(q));
            });
        }
    }
    return health;
}());
exports.health = health;
//# sourceMappingURL=health.js.map