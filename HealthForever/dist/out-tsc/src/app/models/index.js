"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./option"));
__export(require("./question"));
__export(require("./health"));
__export(require("./health-config"));
__export(require("./person"));
//# sourceMappingURL=index.js.map