"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/common/http");
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var health_service_1 = require("../services/health.service");
var UploadmultifilesComponent = /** @class */ (function () {
    function UploadmultifilesComponent(healthService) {
        this.healthService = healthService;
        this.files = [];
    }
    UploadmultifilesComponent.prototype.ngOnInit = function () {
        console.log("upload multiple files");
    };
    UploadmultifilesComponent.prototype.uploadFile = function (file) {
        var formData = new FormData();
        formData.append('files', file.data);
        file.inProgress = true;
        this.healthService.uploadImages(formData).pipe(operators_1.map(function (event) {
            switch (event.type) {
                case http_1.HttpEventType.UploadProgress:
                    file.progress = Math.round(event.loaded * 100 / event.total);
                    break;
                case http_1.HttpEventType.Response:
                    return event;
            }
        }), operators_1.catchError(function (error) {
            file.inProgress = false;
            return rxjs_1.of(file.data.name + " upload failed.");
        })).subscribe(function (event) {
            if (typeof (event) === 'object') {
                console.log(event.body);
            }
        });
    };
    UploadmultifilesComponent.prototype.uploadFiles = function () {
        var _this = this;
        this.fileUpload.nativeElement.value = '';
        this.files.forEach(function (file) {
            _this.uploadFile(file);
        });
    };
    UploadmultifilesComponent.prototype.onClick = function () {
        var _this = this;
        var fileUpload = this.fileUpload.nativeElement;
        fileUpload.onchange = function () {
            for (var index = 0; index < fileUpload.files.length; index++) {
                var file = fileUpload.files[index];
                _this.files.push({ data: file, inProgress: false, progress: 0 });
            }
            _this.uploadFiles();
        };
        fileUpload.click();
    };
    __decorate([
        core_1.ViewChild("fileUpload", { static: false }),
        __metadata("design:type", core_1.ElementRef)
    ], UploadmultifilesComponent.prototype, "fileUpload", void 0);
    UploadmultifilesComponent = __decorate([
        core_1.Component({
            selector: 'app-uploadmultifiles',
            templateUrl: './uploadmultifiles.component.html',
            styleUrls: ['./uploadmultifiles.component.css'],
            providers: [health_service_1.healthService]
        }),
        __metadata("design:paramtypes", [health_service_1.healthService])
    ], UploadmultifilesComponent);
    return UploadmultifilesComponent;
}());
exports.UploadmultifilesComponent = UploadmultifilesComponent;
//# sourceMappingURL=uploadmultifiles.component.js.map