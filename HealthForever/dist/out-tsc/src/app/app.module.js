"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var app_routing_module_1 = require("./app-routing.module");
var forms_1 = require("@angular/forms");
var forms_2 = require("@angular/forms");
var app_component_1 = require("./app.component");
var health_component_1 = require("./health/health.component");
var http_1 = require("@angular/common/http");
var animations_1 = require("@angular/platform-browser/animations");
var material_1 = require("@angular/material");
var material_2 = require("@angular/material");
var material_3 = require("@angular/material");
var user_component_1 = require("./user/user.component");
var home_component_1 = require("./home/home.component");
var action_component_1 = require("./action/action.component");
var noaction_component_1 = require("./noaction/noaction.component");
var ngx_cookie_service_1 = require("ngx-cookie-service");
var uploadfile_component_1 = require("./uploadfile/uploadfile.component");
var uploadmultifiles_component_1 = require("./uploadmultifiles/uploadmultifiles.component");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                health_component_1.healthComponent,
                user_component_1.UserComponent,
                home_component_1.HomeComponent,
                action_component_1.ActionComponent,
                noaction_component_1.NoactionComponent,
                uploadfile_component_1.UploadfileComponent,
                uploadmultifiles_component_1.UploadmultifilesComponent
            ],
            entryComponents: [user_component_1.UserComponent],
            imports: [
                app_routing_module_1.AppRoutingModule,
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                forms_2.ReactiveFormsModule,
                http_1.HttpClientModule,
                material_3.MatSnackBarModule,
                material_2.MatCardModule,
                animations_1.BrowserAnimationsModule,
                material_2.MatRadioModule,
                material_2.MatInputModule,
                material_3.MatFormFieldModule,
                material_3.MatDialogModule,
                platform_browser_1.BrowserModule,
                app_routing_module_1.AppRoutingModule,
                http_1.HttpClientModule,
                animations_1.BrowserAnimationsModule,
                material_1.MatToolbarModule,
                material_2.MatIconModule,
                material_2.MatButtonModule,
                material_2.MatCardModule,
                material_1.MatProgressBarModule,
                material_1.MatToolbarModule
            ],
            providers: [ngx_cookie_service_1.CookieService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map