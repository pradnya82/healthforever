"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var ActionComponent = /** @class */ (function () {
    function ActionComponent(router, route) {
        this.router = router;
        this.route = route;
        this.condition = '';
        this.actionMessage = "Stay Safe";
    }
    ActionComponent.prototype.ngOnInit = function () {
        this.actionMessage = this.route.snapshot.params.message;
        console.log("message from previous page " + this.actionMessage);
        this.condition = this.route.snapshot.params.condition;
        console.log("this home page is loaded using snapshot for " + this.condition);
    };
    ActionComponent.prototype.onClick = function () {
        this.router.navigate(['/health', this.condition]);
    };
    ActionComponent = __decorate([
        core_1.Component({
            selector: 'app-action',
            templateUrl: './action.component.html',
            styleUrls: ['./action.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router, router_2.ActivatedRoute])
    ], ActionComponent);
    return ActionComponent;
}());
exports.ActionComponent = ActionComponent;
//# sourceMappingURL=action.component.js.map