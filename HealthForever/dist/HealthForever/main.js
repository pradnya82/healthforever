(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/action/action.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/action/action.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body>\r\n\r\n    <div class=\"imag\">\r\n        <img class=\"resize\" src=\"assets/sahyadri.png\" alt=\"\">\r\n             \r\n    <h1 class=\"text-center font-weight-Bold\">Forever Healthy</h1>\r\n    <hr />\r\n</div>\r\n    \r\n\r\n<div class=\"text-center\">\r\n    <h2 class=\"row justify-content-center align-items-center\">{{actionMessage}}</h2>\r\n    \r\n    <button  mat-raised-button class=\"btn\" (click)=\"onClick()\" >Check here</button>\r\n    \r\n</div><br>\r\n\r\n</body>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/health/health.component.html":
/*!************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/health/health.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<html>\r\n<div class=\"\">\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-6\">\r\n    <!-- <h3>health Application</h3>  -->\r\n  </div><br><br>\r\n</div>\r\n       \r\n<div class=\"imag\">\r\n  <img class=\"resize\" src=\"assets/lokmanya.png\" alt=\"\">\r\n</div>\r\n\r\n\r\n<div id=\"health\">\r\n  <h1 class=\"text-center font-weight-Bold\">Forever Healthy</h1>\r\n  <hr />\r\n\r\n  <!-- <div>\r\n    <img class= \"imag\" src=\"../../assets/img1.jpg\" alt=\"\">\r\n  </div> -->\r\n  \r\n  <!--\r\n  <div *ngFor=\"let question of filteredQuestions;\" > \r\n    <div *ngIf=\"question.photoSource != '' \">\r\n      <img class= \"imag\" src={{question.photoSource}}/>\r\n    </div>\r\n  </div> \r\n-->\r\n  \r\n  <hr />\r\n\r\n  <div *ngIf=\"mode=='health' && health\">\r\n    <div *ngFor=\"let question of filteredQuestions;\">\r\n\r\n      <h1 class=\"font-weight-normal\">{{pager.index + 1}}.\r\n        <span [innerHTML]=\"question.name\"></span>\r\n      \r\n      </h1><br>\r\n      \r\n      <form #f=\"ngForm\">\r\n      <div *ngIf=\"question.answertype == 'inputText'\">\r\n        <label class=\"\" >\r\n          <mat-form-field class=\"example-full-width\">\r\n            <input #inputText [(ngModel)]=\"question.userAns\" matInput placeholder=\"Write here\" name=\"Text\" required>\r\n          </mat-form-field><br>\r\n        </label>\r\n      </div>\r\n    </form>\r\n\r\n      <div class=\"row text-left options\">\r\n\r\n        <div class=\"col-12\" *ngFor=\"let option of question.options\">\r\n         \r\n          <div *ngIf=\"question.answertype == 'Checkbox'\" >\r\n            <label class=\"round\" [attr.for]=\"option.id\">\r\n              <input #check id=\"{{option.id}}\" type=\"checkbox\" [(ngModel)]=\"option.selected\" (change)=\"onSelect(question, option);\"/> {{option.name}} \r\n              <!-- (change)=\"onSelect(question, option);\" -->\r\n            </label>\r\n          </div>\r\n\r\n          <div *ngIf=\"question.answertype == 'photo'\">\r\n            <label class=\"\" [attr.for]=\"option.id\">\r\n              <div class=\"container\">\r\n                <div class=\"row text-left\">\r\n                    <div class=\"col-md-6 offset-md-0\">\r\n                        <h5>Choose File</h5>            \r\n                        <div class=\"form-group\">\r\n                            <input #imag type=\"file\" name=\"image\" (change)=\"fileProgress($event)\" />\r\n                        </div>\r\n                        <div *ngIf=\"fileUploadProgress\">\r\n                            Upload progress: {{ fileUploadProgress }}\r\n                        </div>\r\n                        <div class=\"image-preview mb-3\" *ngIf=\"previewUrl\">\r\n                            <img [src]=\"previewUrl\" height=\"300\" />                 \r\n                        </div>\r\n              \r\n                        <div class=\"mb-3\" *ngIf=\"uploadedFilePath\">\r\n                            {{uploadedFilePath}}\r\n                        </div>                        \r\n                        <div class=\"form-group\">\r\n                            <button class=\"btn btn-primary\"  (click)=\"onImgSubmit()\">Submit</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n              </div>\r\n            </label>\r\n          </div>\r\n\r\n        </div>\r\n\r\n        <div *ngIf=\"question.answertype == 'Radio'\" class=\"col-12\">\r\n          <mat-radio-group name=\"language\"  class=\"option-radio-group\" [(ngModel)]=\"question.userAns\">\r\n            <mat-radio-button #radi class=\"option-radio-group\" *ngFor=\"let option of question.options\" [value]=\"option.name\" (change)=\"question.score = option.score; question.optionId = option.id \" >\r\n              {{option.name}}\r\n            </mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n\r\n      </div>\r\n    \r\n    <hr />\r\n    <div class=\"text-center\">\r\n      <!-- <button class=\"btn btn-default\" *ngIf=\"config.allowBack\" (click)=\"goTo(0);\">First</button> -->\r\n      <button class=\"btn btn-default\" *ngIf=\"config.allowBack\" (click)=\"goTo(pager.index - 1);\">Previous</button>\r\n      <button *ngIf=\"!(pager.index + 1 == pager.count)\" class=\"btn btn-primary\" (click)=\"goTo(pager.index + 1);\">Next</button>\r\n    </div>\r\n  </div>\r\n  </div>\r\n \r\n  <div class=\"row text-center\" *ngIf=\"mode=='review'\">\r\n    <div class=\"col-4 cursor-pointer\" *ngFor=\"let question of health.questions; let index = index;\">\r\n      <div (click)=\"goTo(index)\" class=\"p-3 mb-2 {{ isAnswered(question) == 'Answered'? 'bg-info': 'bg-warning' }}\">{{index + 1}}. {{ isAnswered(question) }}</div>\r\n    </div>\r\n  </div><hr/>\r\n  \r\n  \r\n  \r\n  <div *ngIf=\"pager.index + 1 == pager.count && !nextAct\" class=\"text-center\">\r\n    <button class=\"btn btn-info\" (click)=\"onSubmit();\">Submit</button>\r\n  </div>\r\n</div>\r\n\r\n<div *ngIf = \"nextAct\" class=\"text-center\">\r\n  <!-- <label for=\"\">temp</label> -->\r\n  <button  mat-raised-button class=\"btn btn-warning\" (click)=\"backToHome();\" >Back Home</button>\r\n <!-- <button  mat-raised-button class=\"btn btn-info\" (click)=\"nextAction();\" >Next Action</button> -->\r\n</div>\r\n\r\n\r\n</div>  \r\n</html>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body>\r\n       \r\n    <div class=\"imag\">\r\n        <img class=\"resize\" src=\"assets/Healthy.jpg\" alt=\"\">\r\n    </div>\r\n    \r\n\r\n<div class=\"text-center\">\r\n    <h1 class=\"row justify-content-center align-items-center\">{{message}}</h1>\r\n    \r\n    <button  mat-raised-button class=\"btn\" (click)=\"onClick()\" >Check here</button>\r\n    \r\n</div><br>\r\n\r\n</body>\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/noaction/noaction.component.html":
/*!****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/noaction/noaction.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<body>\r\n       \r\n  \r\n    <div class=\"imag\">\r\n        <img class=\"resize\" src=\"assets/lokmanya.png\" alt=\"\">\r\n      </div>\r\n      <h1 class=\"text-center font-weight-Bold\">Forever Healthy</h1>         \r\n\r\n    <div class=\"text-center\">\r\n        <h2 class=\"row justify-content-center align-items-center\">{{actionMessage}}</h2>\r\n        \r\n        \r\n        \r\n    </div><br>\r\n    \r\n    </body>\r\n    \r\n    "

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/user/user.component.html":
/*!********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/user/user.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <form [formGroup]=\"registerForm\" (ngSubmit)=\"onSubmit()\"> -->\r\n    <div *ngIf=\"addUser\" class=\"addUser\">\r\n        <h3>Fill Personal Details</h3><br>\r\n\r\n        <mat-form-field class=\"example-full-width\">\r\n            <input matInput  placeholder=\"Name\" name=\"user\" [(ngModel)]=\"data.username\" required> \r\n        </mat-form-field><br>\r\n        \r\n        \r\n        <mat-form-field class=\"example-full-width\">           \r\n            <input matInput placeholder=\"Mobile\" name=\"mobl\" [(ngModel)]=\"data.mobileNo\" pattern=\"[789][0-9]{9}\" required>\r\n        </mat-form-field>\r\n        \r\n        <!-- <div class=\"form-group\">\r\n            <label>Email</label>\r\n            <input type=\"text\" formControlName=\"email\" class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\" />\r\n            <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\r\n                <div *ngIf=\"f.email.errors.required\">Email is required</div>\r\n                <div *ngIf=\"f.email.errors.email\">Email must be a valid email address</div>\r\n            </div>\r\n        </div> --><br><br>\r\n        \r\n        <button #submit mat-raised-button class=\"btn btn-warning\" [mat-dialog-close]=\"data.username + ',' + data.mobileNo\" >Submit</button>\r\n \r\n    </div>\r\n<!-- </form> -->\r\n\r\n    "

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/action/action.component.css":
/*!*********************************************!*\
  !*** ./src/app/action/action.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FjdGlvbi9hY3Rpb24uY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/action/action.component.ts":
/*!********************************************!*\
  !*** ./src/app/action/action.component.ts ***!
  \********************************************/
/*! exports provided: ActionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActionComponent", function() { return ActionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ActionComponent = /** @class */ (function () {
    function ActionComponent(router, route) {
        this.router = router;
        this.route = route;
        this.condition = '';
        this.actionMessage = "Stay Safe";
    }
    ActionComponent.prototype.ngOnInit = function () {
        this.actionMessage = this.route.snapshot.params.message;
        console.log("message from previous page " + this.actionMessage);
        this.condition = this.route.snapshot.params.condition;
        console.log("this home page is loaded using snapshot for " + this.condition);
    };
    ActionComponent.prototype.onClick = function () {
        this.router.navigate(['/health', this.condition]);
    };
    ActionComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    ActionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-action',
            template: __webpack_require__(/*! raw-loader!./action.component.html */ "./node_modules/raw-loader/index.js!./src/app/action/action.component.html"),
            styles: [__webpack_require__(/*! ./action.component.css */ "./src/app/action/action.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], ActionComponent);
    return ActionComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _health_health_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./health/health.component */ "./src/app/health/health.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _action_action_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./action/action.component */ "./src/app/action/action.component.ts");
/* harmony import */ var _noaction_noaction_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./noaction/noaction.component */ "./src/app/noaction/noaction.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'home/:condition', component: _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"] },
    { path: 'action/:condition/:message', component: _action_action_component__WEBPACK_IMPORTED_MODULE_5__["ActionComponent"] },
    { path: 'noaction/:message', component: _noaction_noaction_component__WEBPACK_IMPORTED_MODULE_6__["NoactionComponent"] },
    { path: 'health', component: _health_health_component__WEBPACK_IMPORTED_MODULE_2__["healthComponent"] },
    { path: 'health/:condition', component: _health_health_component__WEBPACK_IMPORTED_MODULE_2__["healthComponent"] },
    { path: 'user', component: _user_user_component__WEBPACK_IMPORTED_MODULE_3__["UserComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            //imports: [RouterModule.forRoot(routes)],
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(router) {
        this.router = router;
    }
    AppComponent.prototype.ngOnInit = function () { };
    AppComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }
    ]; };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _health_health_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./health/health.component */ "./src/app/health/health.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _action_action_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./action/action.component */ "./src/app/action/action.component.ts");
/* harmony import */ var _noaction_noaction_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./noaction/noaction.component */ "./src/app/noaction/noaction.component.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/fesm5/ngx-cookie-service.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















//import { UserComponent } from './user/user.component';

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _health_health_component__WEBPACK_IMPORTED_MODULE_5__["healthComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_9__["UserComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_10__["HomeComponent"],
                _action_action_component__WEBPACK_IMPORTED_MODULE_11__["ActionComponent"],
                _noaction_noaction_component__WEBPACK_IMPORTED_MODULE_12__["NoactionComponent"],
            ],
            entryComponents: [_user_user_component__WEBPACK_IMPORTED_MODULE_9__["UserComponent"]],
            imports: [
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatCardModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatDialogModule"]
            ],
            providers: [ngx_cookie_service__WEBPACK_IMPORTED_MODULE_13__["CookieService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/health/health.component.css":
/*!*********************************************!*\
  !*** ./src/app/health/health.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html{\r\n  margin: 20px;\r\n  height: 100%; \r\n  \r\n}\r\n.mainDiv{\r\n  height: 100%;\r\n  display: flex; \r\n  align-items: center; \r\n  justify-content: center;\r\n}\r\n.imag{\r\n    height: 100px;\r\n    width: 100%;\r\n    \r\n}\r\n.container {\r\n  max-width: 100%;\r\n  margin: 0px auto;\r\n  margin-top: 50px;\r\n}\r\n.textinput {\r\n  float: left;\r\n  width: 100%;\r\n  min-height: 75px;\r\n  outline: none;\r\n  border: 1px solid grey;\r\n}\r\n.hero-image {\r\n    height: 100%;\r\n    width: 100%;\r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n    position: absolute;\r\n  }\r\n.option-radio-group {\r\n    display: inline-flex;\r\n    flex-direction: column;\r\n  }\r\n.option-radio-button {\r\n    margin: 6px;\r\n  }\r\n.example-full-width{\r\n    width: 100%;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaGVhbHRoL2hlYWx0aC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtFQUNaLFlBQVk7O0FBRWQ7QUFDQTtFQUNFLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtBQUN6QjtBQUNBO0lBQ0ksYUFBYTtJQUNiLFdBQVc7O0FBRWY7QUFDQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isc0JBQXNCO0FBQ3hCO0FBRUE7SUFDSSxZQUFZO0lBQ1osV0FBVztJQUNYLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0JBQXNCO0lBQ3RCLGtCQUFrQjtFQUNwQjtBQUVBO0lBQ0Usb0JBQW9CO0lBQ3BCLHNCQUFzQjtFQUN4QjtBQUNBO0lBQ0UsV0FBVztFQUNiO0FBQ0E7SUFDRSxXQUFXO0VBQ2IiLCJmaWxlIjoic3JjL2FwcC9oZWFsdGgvaGVhbHRoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJodG1se1xyXG4gIG1hcmdpbjogMjBweDtcclxuICBoZWlnaHQ6IDEwMCU7IFxyXG4gIFxyXG59XHJcbi5tYWluRGl2e1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4OyBcclxuICBhbGlnbi1pdGVtczogY2VudGVyOyBcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4uaW1hZ3tcclxuICAgIGhlaWdodDogMTAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIFxyXG59XHJcbi5jb250YWluZXIge1xyXG4gIG1heC13aWR0aDogMTAwJTtcclxuICBtYXJnaW46IDBweCBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDUwcHg7XHJcbn1cclxuLnRleHRpbnB1dCB7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWluLWhlaWdodDogNzVweDtcclxuICBvdXRsaW5lOiBub25lO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbn1cclxuICBcclxuLmhlcm8taW1hZ2Uge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB9XHJcblxyXG4gIC5vcHRpb24tcmFkaW8tZ3JvdXAge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIH1cclxuICAub3B0aW9uLXJhZGlvLWJ1dHRvbiB7XHJcbiAgICBtYXJnaW46IDZweDtcclxuICB9IFxyXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGh7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9Il19 */"

/***/ }),

/***/ "./src/app/health/health.component.ts":
/*!********************************************!*\
  !*** ./src/app/health/health.component.ts ***!
  \********************************************/
/*! exports provided: healthComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "healthComponent", function() { return healthComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_health_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/health.service */ "./src/app/services/health.service.ts");
/* harmony import */ var _models_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/index */ "./src/app/models/index.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_SnackbarExtended__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../services/SnackbarExtended */ "./src/app/services/SnackbarExtended.ts");
/* harmony import */ var ngx_cookie_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-cookie-service */ "./node_modules/ngx-cookie-service/fesm5/ngx-cookie-service.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var healthComponent = /** @class */ (function () {
    function healthComponent(healthService, router, http, snackBar, dialog, route, cookieService) {
        this.healthService = healthService;
        this.router = router;
        this.http = http;
        this.snackBar = snackBar;
        this.dialog = dialog;
        this.route = route;
        this.cookieService = cookieService;
        this.nextAct = false;
        this.health = new _models_index__WEBPACK_IMPORTED_MODULE_2__["health"](null);
        this.mode = 'health';
        this.config = {
            'allowBack': true,
            'allowReview': true,
            'autoMove': false,
            'duration': 300,
            'pageSize': 1,
            'requiredAll': false,
            'richText': false,
            'shuffleQuestions': false,
            'shuffleOptions': false,
            'showClock': false,
            'showPager': true,
            'theme': 'none'
        };
        //params: Params;
        this.message = "Stay at home, Stay safe";
        this.condition = 'general';
        this.pager = {
            index: 0,
            size: 1,
            count: 1
        };
        this.fileData = null;
        this.previewUrl = null;
        this.fileUploadProgress = null;
        this.uploadedFilePath = null;
        this.username = this.cookieService.get("userName");
        this.mobileNo = this.cookieService.get("mobileNo");
        this.userId = Number(this.cookieService.get("userId"));
    }
    healthComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.paramMap.subscribe(function (params) {
            _this.condition = params.get("condition"); // Subscription param
            console.log("this page is loaded using subscribe for " + _this.condition);
            console.log("user id from cookie " + _this.cookieService.get("userId"));
            console.log("user name from cookie " + _this.cookieService.get("userName"));
        });
        //this.condition = this.route.snapshot.params.condition;
        this.healthes = this.healthService.getAll();
        this.healthName = this.healthes[0].id;
        this.loadhealth(this.healthName);
    };
    healthComponent.prototype.loadhealth = function (healthName) {
        var _this = this;
        this.pager.index = 0;
        this.healthService.getQuestions(this.condition).subscribe(function (res) {
            _this.health = new _models_index__WEBPACK_IMPORTED_MODULE_2__["health"](res);
            console.log(res);
            _this.pager.count = _this.health.questions.length;
            _this.numOfQues = _this.health.questions.length;
        }, function (error) {
            console.log(error);
        });
        // this.healthService.get(this.healthName).subscribe(res => {
        //   this.health = new health(res);
        //   this.pager.count = this.health.questions.length;
        // });
        this.mode = 'health';
    };
    Object.defineProperty(healthComponent.prototype, "filteredQuestions", {
        get: function () {
            return (this.health.questions) ?
                this.health.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
        },
        enumerable: true,
        configurable: true
    });
    healthComponent.prototype.onSelect = function (question, option) {
        this.answertype = question.answertype;
        var optionName = option.name + ' $';
        question.userAns = question.userAns ? question.userAns : '';
        if (option.selected == true) {
            console.log(option);
            if (question.userAns.search(optionName) === -1) {
                question.userAns += optionName;
            }
        }
        else {
            question.userAns = question.userAns.replace(optionName, "");
        }
        // if (this.config.autoMove) {
        //   this.goTo(this.pager.index + 1);
        // }
    };
    healthComponent.prototype.goTo = function (index) {
        if (index >= 0 && index < this.pager.count) {
            this.pager.index = index;
            this.mode = 'health';
        }
    };
    healthComponent.prototype.onSubmit = function () {
        var _this = this;
        var data = { condition: this.condition, user: { name: null, mobile: null, userId: null }, answers: null };
        if (this.cookieService.get("userId") == null) { }
        var dialogRef = this.dialog.open(_user_user_component__WEBPACK_IMPORTED_MODULE_5__["UserComponent"], {
            width: '400px',
            data: { username: this.username, mobileNo: this.mobileNo }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.nextAct = true;
            var item = result.split(',');
            data.user.name = item[0];
            data.user.mobile = item[1];
            //  data.user.userId=this.cookieService.get("userId");
            _this.cookieService.set("userName", data.user.name);
            _this.cookieService.set("mobileNo", data.user.mobile);
            data.answers = _this.health.questions.map(function (element) {
                return {
                    questionId: element.id,
                    optionId: element.optionId,
                    answer: element.userAns,
                    score: element.score
                };
            });
            console.log(data);
            _this.healthService.getAnswered(data).subscribe(function (response) {
                console.log(response);
                _this.snackBar.openSnackBar(response['responseMessage']);
                _this.nextMessage = response['responseMessage'];
                _this.nextMove = response['nextAction'];
                _this.userId = response['userId'];
                _this.cookieService.set("userId", String(_this.userId));
                console.log("from response:" + _this.nextMove + "," + _this.nextMessage);
                if (_this.nextMove == "undefined" || _this.nextMove == "none") {
                    console.log("next action not available");
                    _this.router.navigate(['/noaction', _this.message]);
                }
                else {
                    console.log("next action available");
                    _this.router.navigate(['/action', _this.nextMove, _this.nextMessage]);
                }
            }, function (error) {
                console.log(error);
            });
        });
        // }else{
        //   const dialogRef = this.dialog.open(UserComponent, {
        //     width: '400px',
        //     data: {username: this.username, mobileNo: this.mobileNo}
        //   });
        return;
    };
    healthComponent.prototype.backToHome = function () {
        return this.router.navigateByUrl("home");
    };
    healthComponent.prototype.nextAction = function () {
        var _this = this;
        if (this.nextMove != "") {
            this.healthService.getNextAct(this.nextMove).subscribe(function (res) {
                _this.nextAct = false;
                console.log(res);
                _this.condition = _this.nextMove;
                console.log("new condition:" + _this.condition);
                _this.router.navigate(['/action', _this.condition, _this.message]);
                //this.health = new health(res);
                // this.pager.count = this.health.questions.length;
            });
        }
    };
    healthComponent.prototype.fileProgress = function (fileInput) {
        this.fileData = fileInput.target.files[0];
        this.preview();
    };
    healthComponent.prototype.preview = function () {
        var _this = this;
        // Show preview 
        var mimeType = this.fileData.type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }
        var reader = new FileReader();
        reader.readAsDataURL(this.fileData);
        reader.onload = function (_event) {
            _this.previewUrl = reader.result;
        };
    };
    healthComponent.prototype.onImgSubmit = function () {
        var _this = this;
        var formData = new FormData();
        formData.append('files', this.fileData);
        this.fileUploadProgress = '0%';
        var url = 'https://us-central1-tutorial-e6ea7.cloudfunctions.net/fileUpload';
        this.http.post(url, formData, {
            reportProgress: true,
            observe: 'events'
        })
            .subscribe(function (events) {
            if (events.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].UploadProgress) {
                _this.fileUploadProgress = Math.round(events.loaded / events.total * 100) + '%';
                console.log(_this.fileUploadProgress);
            }
            else if (events.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpEventType"].Response) {
                _this.fileUploadProgress = '';
                console.log(events.body);
                alert('SUCCESS !!');
            }
        });
    };
    healthComponent.ctorParameters = function () { return [
        { type: _services_health_service__WEBPACK_IMPORTED_MODULE_1__["healthService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"] },
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] },
        { type: _services_SnackbarExtended__WEBPACK_IMPORTED_MODULE_7__["snackbarExtended"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] },
        { type: ngx_cookie_service__WEBPACK_IMPORTED_MODULE_8__["CookieService"] }
    ]; };
    healthComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-health',
            template: __webpack_require__(/*! raw-loader!./health.component.html */ "./node_modules/raw-loader/index.js!./src/app/health/health.component.html"),
            providers: [_services_health_service__WEBPACK_IMPORTED_MODULE_1__["healthService"]],
            styles: [__webpack_require__(/*! ./health.component.css */ "./src/app/health/health.component.css")]
        }),
        __metadata("design:paramtypes", [_services_health_service__WEBPACK_IMPORTED_MODULE_1__["healthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"],
            _services_SnackbarExtended__WEBPACK_IMPORTED_MODULE_7__["snackbarExtended"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            ngx_cookie_service__WEBPACK_IMPORTED_MODULE_8__["CookieService"]])
    ], healthComponent);
    return healthComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-center{\r\n    color: green;\r\n    font-weight: bold;\r\n    /* height: 100%; \r\n    display: flex; \r\n    align-items: center; \r\n    justify-content: center; */\r\n}\r\n.links{\r\n    text-align: center;\r\n    margin-bottom: auto;\r\n}\r\n.link{\r\n    font-size: 12px;\r\n}\r\nbody{\r\nmargin: 20PX;\r\n}\r\nimg.resize {\r\n    width:250px;\r\n    height:350px;\r\n}\r\n.imag {\r\n    text-align: center;\r\n}\r\n.btn{\r\n    background-color: aquamarine\r\n}\r\n.icon{\r\n    height: 25px;\r\n    width: 25px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCOzs7OEJBRzBCO0FBQzlCO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxlQUFlO0FBQ25CO0FBQ0E7QUFDQSxZQUFZO0FBQ1o7QUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFDQTtJQUNJO0FBQ0o7QUFDQTtJQUNJLFlBQVk7SUFDWixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0LWNlbnRlcntcclxuICAgIGNvbG9yOiBncmVlbjtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgLyogaGVpZ2h0OiAxMDAlOyBcclxuICAgIGRpc3BsYXk6IGZsZXg7IFxyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgKi9cclxufVxyXG4ubGlua3N7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiBhdXRvO1xyXG59XHJcbi5saW5re1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG59XHJcbmJvZHl7XHJcbm1hcmdpbjogMjBQWDtcclxufVxyXG5pbWcucmVzaXplIHtcclxuICAgIHdpZHRoOjI1MHB4O1xyXG4gICAgaGVpZ2h0OjM1MHB4O1xyXG59XHJcbi5pbWFnIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYnRue1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogYXF1YW1hcmluZVxyXG59XHJcbi5pY29ue1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgd2lkdGg6IDI1cHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomeComponent = /** @class */ (function () {
    function HomeComponent(router, route) {
        this.router = router;
        this.route = route;
        this.condition = '';
        this.message = "Stay Safe";
    }
    HomeComponent.prototype.ngOnInit = function () {
        this.condition = this.route.snapshot.params.condition;
        console.log("this home page is loaded using snapshot for " + this.condition);
    };
    HomeComponent.prototype.onClick = function () {
        this.router.navigate(['/health', this.condition]);
    };
    HomeComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.component.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/models/health-config.ts":
/*!*****************************************!*\
  !*** ./src/app/models/health-config.ts ***!
  \*****************************************/
/*! exports provided: healthConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "healthConfig", function() { return healthConfig; });
var healthConfig = /** @class */ (function () {
    function healthConfig(data) {
        data = data || {};
        this.allowBack = data.allowBack;
        this.allowReview = data.allowReview;
        this.autoMove = data.autoMove;
        this.duration = data.duration;
        this.pageSize = data.pageSize;
        this.requiredAll = data.requiredAll;
        this.richText = data.richText;
        this.shuffleQuestions = data.shuffleQuestions;
        this.shuffleOptions = data.shuffleOptions;
        this.showClock = data.showClock;
        this.showPager = data.showPager;
    }
    healthConfig.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return healthConfig;
}());



/***/ }),

/***/ "./src/app/models/health.ts":
/*!**********************************!*\
  !*** ./src/app/models/health.ts ***!
  \**********************************/
/*! exports provided: health */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "health", function() { return health; });
/* harmony import */ var _health_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./health-config */ "./src/app/models/health-config.ts");
/* harmony import */ var _question__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./question */ "./src/app/models/question.ts");


var health = /** @class */ (function () {
    function health(data) {
        var _this = this;
        if (data) {
            this.userName = "";
            this.mobileNo = 0;
            this.id = data.questionId;
            this.name = data.responseMessage;
            this.config = new _health_config__WEBPACK_IMPORTED_MODULE_0__["healthConfig"](data.config);
            this.questions = [];
            this.answered = [];
            this.person = [];
            data.response.forEach(function (q) {
                _this.questions.push(new _question__WEBPACK_IMPORTED_MODULE_1__["Question"](q));
            });
        }
    }
    health.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return health;
}());



/***/ }),

/***/ "./src/app/models/index.ts":
/*!*********************************!*\
  !*** ./src/app/models/index.ts ***!
  \*********************************/
/*! exports provided: Option, Question, health, healthConfig, person */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _option__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./option */ "./src/app/models/option.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Option", function() { return _option__WEBPACK_IMPORTED_MODULE_0__["Option"]; });

/* harmony import */ var _question__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./question */ "./src/app/models/question.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Question", function() { return _question__WEBPACK_IMPORTED_MODULE_1__["Question"]; });

/* harmony import */ var _health__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./health */ "./src/app/models/health.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "health", function() { return _health__WEBPACK_IMPORTED_MODULE_2__["health"]; });

/* harmony import */ var _health_config__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./health-config */ "./src/app/models/health-config.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "healthConfig", function() { return _health_config__WEBPACK_IMPORTED_MODULE_3__["healthConfig"]; });

/* harmony import */ var _person__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./person */ "./src/app/models/person.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "person", function() { return _person__WEBPACK_IMPORTED_MODULE_4__["person"]; });








/***/ }),

/***/ "./src/app/models/option.ts":
/*!**********************************!*\
  !*** ./src/app/models/option.ts ***!
  \**********************************/
/*! exports provided: Option */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Option", function() { return Option; });
var Option = /** @class */ (function () {
    function Option(data) {
        data = data || {};
        this.id = data.optionId;
        this.name = data.option;
        this.questionId = data.questionId;
        this.score = data.score;
    }
    Option.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return Option;
}());



/***/ }),

/***/ "./src/app/models/person.ts":
/*!**********************************!*\
  !*** ./src/app/models/person.ts ***!
  \**********************************/
/*! exports provided: person */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "person", function() { return person; });
var person = /** @class */ (function () {
    function person() {
    }
    return person;
}());



/***/ }),

/***/ "./src/app/models/question.ts":
/*!************************************!*\
  !*** ./src/app/models/question.ts ***!
  \************************************/
/*! exports provided: Question */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Question", function() { return Question; });
/* harmony import */ var _option__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./option */ "./src/app/models/option.ts");

var Question = /** @class */ (function () {
    function Question(data) {
        var _this = this;
        data = data || {};
        this.id = data.questionId;
        this.name = data.question;
        this.answertype = data.answerType;
        this.options = [];
        this.condition = data.condition;
        this.photoSource = data.photoSource;
        this.userAns = data.userAns;
        this.optionId = data.optionId;
        data.options.forEach(function (o) {
            _this.options.push(new _option__WEBPACK_IMPORTED_MODULE_0__["Option"](o));
        });
    }
    Question.ctorParameters = function () { return [
        { type: undefined }
    ]; };
    return Question;
}());



/***/ }),

/***/ "./src/app/noaction/noaction.component.css":
/*!*************************************************!*\
  !*** ./src/app/noaction/noaction.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL25vYWN0aW9uL25vYWN0aW9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/noaction/noaction.component.ts":
/*!************************************************!*\
  !*** ./src/app/noaction/noaction.component.ts ***!
  \************************************************/
/*! exports provided: NoactionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoactionComponent", function() { return NoactionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NoactionComponent = /** @class */ (function () {
    function NoactionComponent(router, route) {
        this.router = router;
        this.route = route;
        this.condition = '';
        this.actionMessage = "Stay Safe";
    }
    NoactionComponent.prototype.ngOnInit = function () {
        this.actionMessage = this.route.snapshot.params.message;
        console.log("message from previous page " + this.actionMessage);
    };
    NoactionComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"] }
    ]; };
    NoactionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-noaction',
            template: __webpack_require__(/*! raw-loader!./noaction.component.html */ "./node_modules/raw-loader/index.js!./src/app/noaction/noaction.component.html"),
            styles: [__webpack_require__(/*! ./noaction.component.css */ "./src/app/noaction/noaction.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], NoactionComponent);
    return NoactionComponent;
}());



/***/ }),

/***/ "./src/app/services/SnackbarExtended.ts":
/*!**********************************************!*\
  !*** ./src/app/services/SnackbarExtended.ts ***!
  \**********************************************/
/*! exports provided: snackbarExtended */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "snackbarExtended", function() { return snackbarExtended; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
// Provides snackBar configuration globally
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var snackbarExtended = /** @class */ (function () {
    function snackbarExtended(snackBar) {
        this.snackBar = snackBar;
    }
    // this function will open up snackbar on top position with custom background color (defined in css)
    snackbarExtended.prototype.openSnackBar = function (message) {
        this.snackBar.open(message, undefined, {
            duration: 3000,
            verticalPosition: 'top',
            panelClass: ['white-snackbar'],
        });
    };
    snackbarExtended.ctorParameters = function () { return [
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSnackBar"] }
    ]; };
    snackbarExtended = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatSnackBar"]])
    ], snackbarExtended);
    return snackbarExtended;
}());



/***/ }),

/***/ "./src/app/services/health.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/health.service.ts ***!
  \********************************************/
/*! exports provided: healthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "healthService", function() { return healthService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var healthService = /** @class */ (function () {
    function healthService(http) {
        this.http = http;
        //baseUrl = 'http://localhost:8080/quiz/rest/'; 
        this.baseUrl = 'http://52.66.140.20:8080/quiz/rest/';
    }
    healthService.prototype.getQuestions = function (condition) {
        var Url = this.baseUrl + condition;
        return this.http.get(Url);
    };
    healthService.prototype.getAnswered = function (jsonData) {
        var Url = this.baseUrl + 'answers';
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]({
            'Content-Type': 'application/json',
        });
        //
        //console.log(jsonData);
        // console.log(body);     
        return this.http.post(Url, jsonData, { headers: headers });
    };
    healthService.prototype.get = function (url) {
        return this.http.get(url);
    };
    healthService.prototype.getNextAct = function (nextMove) {
        var Url = this.baseUrl + nextMove;
        return this.http.get(Url);
    };
    healthService.prototype.getAll = function () {
        return [
            { id: 'data/javascript.json', name: 'JavaScript' },
            { id: 'data/aspnet.json', name: 'Asp.Net' },
            { id: 'data/csharp.json', name: 'C Sharp' },
            { id: 'data/designPatterns.json', name: 'Design Patterns' }
        ];
    };
    healthService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }
    ]; };
    healthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], healthService);
    return healthService;
}());



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-full-width{    \r\n  width: 100%;\r\n  max-width: 500px;\r\n  box-sizing: border-box;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci91c2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFXO0VBQ1gsZ0JBQWdCO0VBQ2hCLHNCQUFzQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvdXNlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtZnVsbC13aWR0aHsgICAgXHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgbWF4LXdpZHRoOiA1MDBweDtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var UserComponent = /** @class */ (function () {
    function UserComponent(formBuilder, dialog, dialogRef, data) {
        this.formBuilder = formBuilder;
        this.dialog = dialog;
        this.dialogRef = dialogRef;
        this.data = data;
        this.addUser = true;
    }
    // registerForm: FormGroup;
    // submitted = false;
    UserComponent.prototype.ngOnInit = function () {
        // this.registerForm = this.formBuilder.group({
        //   email: ['', [Validators.required, Validators.email]],
        //   password: ['', [Validators.required, Validators.minLength(6)]],
        //   acceptTerms: [false, Validators.requiredTrue]
        // });
    };
    UserComponent.ctorParameters = function () { return [
        { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"] },
        { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"] },
        { type: undefined, decorators: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"], args: [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"],] }] }
    ]; };
    UserComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user',
            template: __webpack_require__(/*! raw-loader!./user.component.html */ "./node_modules/raw-loader/index.js!./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")]
        }),
        __param(3, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"], Object])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\InnoshriDev\PopulationHealth\FrontEnd\healthforever\HealthForever\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map