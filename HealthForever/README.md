# HealthForever

A general purpose health application developed in angular 6 that can be used for multiple purpose.
+
+For detailed documentation, please go to: https://www.codeproject.com/Articles/1167451/health-Application-in-Angular

### Please raise a pull request for any bug fix or if you have implemented a new feature and feel it is useful.
